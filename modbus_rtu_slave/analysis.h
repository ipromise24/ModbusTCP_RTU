#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <QObject>
#include <QSettings>
#include <QDebug>

#define CRC_FLAG 0

typedef enum RTU_MODBUS_STATE
{
    //功能码错误
   MB_SLAVE_STATE_FUNCTION_ERROR = 1,
    //数据值错误
   MB_SLAVE_STATE_DATA_ERROR = 2,
    //数据地址错误
   MB_SLAVE_STATE_DATAADDR_ERROR = 3,
    //从机地址
   MB_SLAVE_STATE__ADDR_ERROR = 4,
    //正确帧
   MB_SLAVE_STATE_PACKET_PROCESS = 5,
    //其他
   MB_SLAVE_STATE_PACKET_OTHER = 6,
}MB_satae;

class analysis: public QObject
{
    Q_OBJECT
public:
   explicit analysis();

signals:
    void toUishowMsg(QString errorMsg);
    void analysis_over(QByteArray ba);
    void toUishowMsgPack(QByteArray packMsg);
    void ShowMsgString(QString error);//显示错误信息
    void ShowMsgQByteArray(QByteArray ba);//显示数组
     void wirtTablec(quint16 num, quint16 satrtaddr,QString bac);
     void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> bar);

public slots:
        void GetData0X01(QByteArray &coilsDataArr,quint16 BeginAddress,quint16 Number);
        void WriteData0X0F(quint16 satrt,QString CoilData);
        QString HexByteArrayToBinString(QByteArray DataArray);
         void ParseResponseMessage(QByteArray msg,quint8 addr);

private:
    //ini文件对象
    QSettings* readseting=nullptr;
    //缓存
    QByteArray recvModbusmsg;
    QByteArray sendModbusmsg;
    quint8  mb_addr;
    quint16 mb_startaddr,mb_num;
    quint16 mb_code;
    QVector<quint16> msg;
     QVector<quint16> bar;
    QString bac;
    quint16 wnum;
    QString errorMsg;
    quint8 len;

    quint16 crc16ForModbus(const QByteArray &data,int flag);
    //01功能码解析
    void analysis01();
    //03功能码解析
    void analysis03();
    //0f功能码解析
    void analysis0f();
    //10功能码解析
    void analysis10();
    //读出线圈并显示
    void ReadCoilPackMsgToShow(quint16 startaddr,quint16 num,QByteArray msg);
    //读出寄存器并显示
    void ReadRegsPackMsgToShow(quint16 startaddr,quint16 num,QVector<quint16> msg);
    //写入线圈并提示
    void WirteCoilPackMsgToShow(quint16 startaddr,quint16 num,QByteArray msg);
    //写入寄存器并显示
    void WirteRegsPackMsgToShow(quint16 startaddr,quint16 num,QVector<quint16> msg);
    //解析报文查询状态
    MB_satae parse_Modbus_Msg(QByteArray msg);
    //状态处理
    void parse_Modbus_MB_satae(MB_satae satae);
    //异常码01处理
    void abnormal_01();
    //异常码02处理
     void abnormal_02();
    //异常码03处理
     void abnormal_03();
     void Reverse(char str[]);
};

#endif // ANALYSIS_H
