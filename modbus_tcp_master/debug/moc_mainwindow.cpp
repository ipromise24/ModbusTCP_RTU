/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[35];
    char stringdata0[446];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 10), // "hide_SLOTS"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 14), // "table_dataInit"
QT_MOC_LITERAL(4, 38, 10), // "wirtTablec"
QT_MOC_LITERAL(5, 49, 3), // "num"
QT_MOC_LITERAL(6, 53, 9), // "satrtaddr"
QT_MOC_LITERAL(7, 63, 3), // "bac"
QT_MOC_LITERAL(8, 67, 10), // "wirtTabler"
QT_MOC_LITERAL(9, 78, 16), // "QVector<quint16>"
QT_MOC_LITERAL(10, 95, 3), // "bar"
QT_MOC_LITERAL(11, 99, 4), // "init"
QT_MOC_LITERAL(12, 104, 14), // "ReadOutIniData"
QT_MOC_LITERAL(13, 119, 23), // "on_coilsetepbtn_clicked"
QT_MOC_LITERAL(14, 143, 22), // "on_regsetupbtn_clicked"
QT_MOC_LITERAL(15, 166, 19), // "on_clearbtn_clicked"
QT_MOC_LITERAL(16, 186, 21), // "on_connectbtn_clicked"
QT_MOC_LITERAL(17, 208, 19), // "on_send_msg_clicked"
QT_MOC_LITERAL(18, 228, 7), // "showMsg"
QT_MOC_LITERAL(19, 236, 3), // "msg"
QT_MOC_LITERAL(20, 240, 9), // "m_ReadMsg"
QT_MOC_LITERAL(21, 250, 15), // "ClientReadError"
QT_MOC_LITERAL(22, 266, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(23, 295, 12), // "packageSartc"
QT_MOC_LITERAL(24, 308, 4), // "addr"
QT_MOC_LITERAL(25, 313, 7), // "funcode"
QT_MOC_LITERAL(26, 321, 9), // "startaddr"
QT_MOC_LITERAL(27, 331, 2), // "ba"
QT_MOC_LITERAL(28, 334, 4), // "regs"
QT_MOC_LITERAL(29, 339, 18), // "TCPAnalysisMessage"
QT_MOC_LITERAL(30, 358, 12), // "MessageArray"
QT_MOC_LITERAL(31, 371, 10), // "resendShow"
QT_MOC_LITERAL(32, 382, 19), // "on_setupbtn_clicked"
QT_MOC_LITERAL(33, 402, 21), // "on_transfiles_clicked"
QT_MOC_LITERAL(34, 424, 21) // "on_historyBtn_clicked"

    },
    "MainWindow\0hide_SLOTS\0\0table_dataInit\0"
    "wirtTablec\0num\0satrtaddr\0bac\0wirtTabler\0"
    "QVector<quint16>\0bar\0init\0ReadOutIniData\0"
    "on_coilsetepbtn_clicked\0on_regsetupbtn_clicked\0"
    "on_clearbtn_clicked\0on_connectbtn_clicked\0"
    "on_send_msg_clicked\0showMsg\0msg\0"
    "m_ReadMsg\0ClientReadError\0"
    "QAbstractSocket::SocketError\0packageSartc\0"
    "addr\0funcode\0startaddr\0ba\0regs\0"
    "TCPAnalysisMessage\0MessageArray\0"
    "resendShow\0on_setupbtn_clicked\0"
    "on_transfiles_clicked\0on_historyBtn_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x08 /* Private */,
       3,    0,  115,    2, 0x08 /* Private */,
       4,    3,  116,    2, 0x08 /* Private */,
       8,    3,  123,    2, 0x08 /* Private */,
      11,    0,  130,    2, 0x08 /* Private */,
      12,    0,  131,    2, 0x08 /* Private */,
      13,    0,  132,    2, 0x08 /* Private */,
      14,    0,  133,    2, 0x08 /* Private */,
      15,    0,  134,    2, 0x08 /* Private */,
      16,    0,  135,    2, 0x08 /* Private */,
      17,    0,  136,    2, 0x08 /* Private */,
      18,    1,  137,    2, 0x08 /* Private */,
      20,    0,  140,    2, 0x08 /* Private */,
      21,    1,  141,    2, 0x08 /* Private */,
      23,    6,  144,    2, 0x08 /* Private */,
      29,    1,  157,    2, 0x08 /* Private */,
      31,    0,  160,    2, 0x08 /* Private */,
      32,    0,  161,    2, 0x08 /* Private */,
      33,    0,  162,    2, 0x08 /* Private */,
      34,    0,  163,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::UShort, QMetaType::UShort, QMetaType::QString,    5,    6,    7,
    QMetaType::Void, QMetaType::UShort, QMetaType::UShort, 0x80000000 | 9,    5,    6,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   19,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,    2,
    QMetaType::Void, QMetaType::UChar, QMetaType::UChar, QMetaType::UShort, QMetaType::UShort, QMetaType::QByteArray, 0x80000000 | 9,   24,   25,   26,    5,   27,   28,
    QMetaType::Bool, QMetaType::QByteArray,   30,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->hide_SLOTS(); break;
        case 1: _t->table_dataInit(); break;
        case 2: _t->wirtTablec((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 3: _t->wirtTabler((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< QVector<quint16>(*)>(_a[3]))); break;
        case 4: _t->init(); break;
        case 5: _t->ReadOutIniData(); break;
        case 6: _t->on_coilsetepbtn_clicked(); break;
        case 7: _t->on_regsetupbtn_clicked(); break;
        case 8: _t->on_clearbtn_clicked(); break;
        case 9: _t->on_connectbtn_clicked(); break;
        case 10: _t->on_send_msg_clicked(); break;
        case 11: _t->showMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->m_ReadMsg(); break;
        case 13: _t->ClientReadError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 14: _t->packageSartc((*reinterpret_cast< quint8(*)>(_a[1])),(*reinterpret_cast< quint8(*)>(_a[2])),(*reinterpret_cast< quint16(*)>(_a[3])),(*reinterpret_cast< quint16(*)>(_a[4])),(*reinterpret_cast< QByteArray(*)>(_a[5])),(*reinterpret_cast< QVector<quint16>(*)>(_a[6]))); break;
        case 15: { bool _r = _t->TCPAnalysisMessage((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 16: _t->resendShow(); break;
        case 17: _t->on_setupbtn_clicked(); break;
        case 18: _t->on_transfiles_clicked(); break;
        case 19: _t->on_historyBtn_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<quint16> >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 5:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<quint16> >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
