/********************************************************************************
** Form generated from reading UI file 'dialogd.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGD_H
#define UI_DIALOGD_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DialogD
{
public:
    QGridLayout *gridLayout_4;
    QLabel *label_4;
    QWidget *widget;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *addrline;
    QLabel *label_2;
    QLineEdit *startaddrline;
    QLabel *label_3;
    QLineEdit *numline;
    QPushButton *start_btn;
    QPushButton *over;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QTableWidget *tableregs;

    void setupUi(QDialog *DialogD)
    {
        if (DialogD->objectName().isEmpty())
            DialogD->setObjectName(QString::fromUtf8("DialogD"));
        DialogD->resize(391, 434);
        gridLayout_4 = new QGridLayout(DialogD);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_4 = new QLabel(DialogD);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_4->addWidget(label_4, 0, 0, 1, 1);

        widget = new QWidget(DialogD);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_3 = new QGridLayout(widget);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox = new QGroupBox(widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        addrline = new QLineEdit(groupBox);
        addrline->setObjectName(QString::fromUtf8("addrline"));

        gridLayout->addWidget(addrline, 0, 1, 1, 2);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        startaddrline = new QLineEdit(groupBox);
        startaddrline->setObjectName(QString::fromUtf8("startaddrline"));

        gridLayout->addWidget(startaddrline, 1, 1, 1, 2);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        numline = new QLineEdit(groupBox);
        numline->setObjectName(QString::fromUtf8("numline"));

        gridLayout->addWidget(numline, 2, 1, 1, 2);

        start_btn = new QPushButton(groupBox);
        start_btn->setObjectName(QString::fromUtf8("start_btn"));

        gridLayout->addWidget(start_btn, 3, 0, 1, 1);

        over = new QPushButton(groupBox);
        over->setObjectName(QString::fromUtf8("over"));

        gridLayout->addWidget(over, 3, 1, 1, 2);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(widget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tableregs = new QTableWidget(groupBox_2);
        if (tableregs->columnCount() < 2)
            tableregs->setColumnCount(2);
        if (tableregs->rowCount() < 123)
            tableregs->setRowCount(123);
        tableregs->setObjectName(QString::fromUtf8("tableregs"));
        tableregs->setRowCount(123);
        tableregs->setColumnCount(2);

        gridLayout_2->addWidget(tableregs, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 1, 0, 1, 1);


        gridLayout_4->addWidget(widget, 1, 0, 1, 1);


        retranslateUi(DialogD);

        QMetaObject::connectSlotsByName(DialogD);
    } // setupUi

    void retranslateUi(QDialog *DialogD)
    {
        DialogD->setWindowTitle(QCoreApplication::translate("DialogD", "Dialog", nullptr));
        label_4->setText(QCoreApplication::translate("DialogD", "\345\212\237\350\203\275\347\240\20110\350\256\276\347\275\256", nullptr));
        groupBox->setTitle(QCoreApplication::translate("DialogD", "\350\276\223\345\205\245\357\274\232", nullptr));
        label->setText(QCoreApplication::translate("DialogD", "\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232", nullptr));
        addrline->setPlaceholderText(QCoreApplication::translate("DialogD", "\350\276\223\345\205\245\344\273\216\346\234\272\345\234\260\345\235\200(1-247)", nullptr));
        label_2->setText(QCoreApplication::translate("DialogD", "\350\265\267\345\247\213\345\234\260\345\235\200\357\274\232", nullptr));
        startaddrline->setPlaceholderText(QCoreApplication::translate("DialogD", "\350\276\223\345\205\245\350\265\267\345\247\213\345\234\260\345\235\200(0-65535)", nullptr));
        label_3->setText(QCoreApplication::translate("DialogD", "\345\257\204\345\255\230\345\231\250\346\225\260\351\207\217\357\274\232", nullptr));
        numline->setPlaceholderText(QCoreApplication::translate("DialogD", "\350\276\223\345\205\245\346\225\260\351\207\217(1-123)", nullptr));
        start_btn->setText(QCoreApplication::translate("DialogD", "\350\256\276\347\275\256", nullptr));
        over->setText(QCoreApplication::translate("DialogD", "\345\256\214\346\210\220", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("DialogD", "\345\257\204\345\255\230\345\231\250\350\256\276\345\200\274\357\274\232", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DialogD: public Ui_DialogD {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGD_H
